module.exports.insertNoteErrors = (err) => {
    let errors = { title: '', content: '' }

    console.log(err.message)

    if (err.message.includes('title') && err.message.includes('longer')) {
        let max = err.errors.notes.errors.title.properties.maxlength
        errors.title = `Le titre doit faire ${max} caractères maximum`
    }

    if (err.message.includes('content') && err.message.includes('longer')) {
        let max = err.errors.notes.errors.content.properties.maxlength
        errors.content = `Le contenu doit faire ${max} caractères maximum`
    }

    return errors
}