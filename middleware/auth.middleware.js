const jwt = require('jsonwebtoken')
const UserModel = require('../models/user.model')

module.exports.checkUser = (req, res, next) => {
    const token = req.cookies.jwt

    if (token) {
        jwt.verify(token, process.env.JWT_SECRET, async (err, decodedToken) => {
            if (err) {
                res.locals.user = null
                next()
            } else {
                let user = await UserModel.findById(decodedToken.id)
                console.log(decodedToken.id)
                res.locals.user = user
                console.log(user)
                next()
            }
        })
    } else {
        res.locals.user = null
        next()
    }
}

module.exports.requireAuth = (req, res, next) => {
    const token = req.cookies.jwt

    if (token) {
        jwt.verify(token, process.env.JWT_SECRET, async (err, decodedToken) => {
            if (err) {
                console.log(err)
            } else {
                console.log(decodedToken.id)
                next()
            }
        })
    } else {
        console.log('No token')
    }
}

module.exports.privateAccess = (req, res, next) => {
    // Récupération du token
    const token = req.cookies.jwt
    // Décodage du token 
    if (token) {
        jwt.verify(token, process.env.JWT_SECRET, async (err, decodedToken) => {
            if (err) {
                res.locals.user = null
                return res.status(401).send('Unauthorized')
            } else {
                let user = await UserModel.findById(decodedToken.id)

                if (!user) {
                    return res.status(401).send('Unauthorized')
                }

                console.log(decodedToken.id)
                if (user.id !== req.params.id) {
                    return res.status(401).send('Unauthorized')
                }
                res.locals.user = user
                next()
            }
        })
    } else {
        res.locals.user = null
        return res.status(401).send('Unauthorized')
    }
}