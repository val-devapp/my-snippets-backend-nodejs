const mongoose = require("mongoose")
const validator = require("validator")
const bcrypt = require("bcryptjs")

const userSchema = new mongoose.Schema(
    {
        username: {
            type: String,
            required: true,
            minlength: 3,
            maxlength: 55,
            unique: true,
            trimp: true,
        },
        email: {
            type: String,
            required: true,
            validate: [validator.isEmail],
            unique: true,
            lowercase: true,
            trim: true,
        },
        password: {
            type: String,
            required: true,
            maxlength: 1024,
            minlength: 6,
        },
        passwordConfirm: {
            type: String,
            required: [true, "Retype your password."],
            validate: {
                validator: function (el) {
                    return el === this.password
                },
                message: "Passwords don't match.",
            },
        },
        notes: {
            type: [
                {
                    _id: {
                        type: mongoose.Schema.Types.ObjectId,
                        index: true,
                        required: true,
                        auto: true,
                    },
                    title: {
                        type: String,
                        default: "",
                        maxlength: 200,
                    },
                    content: {
                        type: String,
                        default: "",
                        maxlength: 3900,
                    },
                    createdDate: {
                        type: Date,
                        default: Date.now,
                    },
                },
            ],
            required: true,
            default: [],
        },
    },
    {
        timestamps: true,
    }
)

// Play function before save into db
userSchema.pre("save", async function (next) {
    const salt = await bcrypt.genSalt()
    this.password = await bcrypt.hash(this.password, salt)

    //remove the passwordConfirmed field
    this.passwordConfirm = undefined
    next()
})

userSchema.statics.login = async function (email, password) {
    console.log(email)
    const user = await this.findOne({ email })
    if (user) {
        const auth = await bcrypt.compare(password, user.password)
        if (auth) {
            return user
        }
        throw Error("Incorrect password")
    }
    throw Error("Incorrect email")
}

const UserModel = mongoose.model("user", userSchema)

module.exports = UserModel
